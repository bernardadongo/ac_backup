//
//  process_data.c
//  ac_project
//
//  Created by Bernard Adongo on 30/04/2022.
//
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include "ac_server.h"
#include "process_data.h"
#include "protocol.h"
#include "utils.h"

/*
 1. Queue the packet for processing
 2. Get received & saved values into server response
 3. Return server response
 */

void enqueue_packet(ConnectionInfo *ci, void *buf, size_t buf_len);

int process_packet(int conn_idx, char *in_buf, char **out_buf, size_t in_buf_len){
    
    ConnectionInfo *ci = &conn_infos[conn_idx];
    ProgressInfo  *pi = ci->pi;
    
    if (ci->packet_type == PACKET_TYPE_TOP){
        MessageHeader *msg_hdr = (MessageHeader *)in_buf;
//        dbg_print("process_packet(): conn: [%d], top packet: %llX [%zu bytes], buf_size: %d\n",
//               conn_idx, msg_hdr->signature, in_buf_len, BUF_SIZE);
//        
        //Pass the packet and update the progress info
        pi->received = msg_hdr->serial_number;
    }
    
    //enqueue packet
    enqueue_packet(ci, in_buf, in_buf_len);
    
    if (in_buf_len < BUF_SIZE){
        if (ci->packet_type==PACKET_TYPE_TOP){
            //next packet will be bottom
            ci->packet_type = PACKET_TYPE_BOTTOM;
            //WE RETRIEVE BOTTOM HALF BEFORE RESPONDING TO SERVER
            //dbg_print("packet_top: connection: [%d]\n", conn_idx);
            return 0;
        }
        else// this was a bottom packet, so next will be top
            //dbg_print("packet_bottom: connection: [%d]\n", conn_idx);

        ci->packet_type = PACKET_TYPE_TOP;
    }
   
    return 0;
}

void enqueue_packet(ConnectionInfo *ci, void *buf, size_t buf_len)
{
    ProgressInfo *pi = ci->pi;
    
    int next_buf_idx = (pi->packet_buf_idx + 1);
    next_buf_idx %= PACKETS_SIZE;
    
    //dbg_print("enqueue_packet(): [%d] pi->packet_buf_idx:  %d\n", pi->idx, next_buf_idx);
    //FIRST prep the packet
    Packet *packet = &pi->packets[next_buf_idx];
    packet->data = buf;
    packet->size = buf_len;
    packet->serial = pi->received;
    packet->type = ci->packet_type;
    
    //THEN update the index
    pi->packet_buf_idx = next_buf_idx;
}
