//
//  ac_client.c
//  ac_project
//
//  Created by Bernard Adongo on 29/04/2022.
//

#include <unistd.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

#include <pthread.h>

#include <event2/event.h>
#include <event2/thread.h>
#include <event2/buffer.h>
#include <event2/bufferevent.h>
#include <event2/util.h>

#include "ac_client.h"
#include "utils.h"
#include "messages.h"

#define SA struct sockaddr

void do_message_function(ClientThreadInfo *cti);
uint64_t get_stream_id(ServerResponse *sr);
void send_protocol_message(int sock_fd, char *protocol,
                           uint64_t signature, ServerResponse *resp);
void wait_for_free_memory(ClientThreadInfo *cti);


static void conn_writecb(struct bufferevent *, void *);
static void conn_readcb(struct bufferevent *, void *);
static void conn_eventcb(struct bufferevent *, short, void *);

int main(int argc, char *argv[])
{
    ClientThreadInfo ctis[THREADS_NR];
    struct sockaddr_in servaddr[THREADS_NR];
    
    evthread_use_pthreads();

    int *sock_fd;
    int *thread_id;
    char server_ip[100];
    int server_port;

    snprintf(server_ip, 100, SERVER_IP);
    server_port = SERVER_PORT;
    if (argc > 2)
      server_port = atoi(argv[2]);
    else if (argc > 1)
      snprintf(server_ip, 100, "%s", argv[1]);

    dbg_print("ac_client %s %d\n", server_ip, server_port);

    
    // socket create and verification - outside the threads
    for (int i=0; i<THREADS_NR; i++){
        //init object
        ctis[i].idx = i;
        ctis[i].messages_head = ctis[i].messages_tail = NULL;
        ctis[i].held_messages_count = 0;
        ctis[i].thread_id = 0;
        ctis[i].stream_id = 0;
        ctis[i].sent_messages_idx = -1;
        ctis[i].status = CLIENT_STATUS_INIT;


        
        sock_fd = &(ctis[i].sock_fd);
        
        *sock_fd = socket(AF_INET, SOCK_STREAM, 0);
        if (*sock_fd == -1) {
            printf("socket creation failed...\n");
            exit(0);
        }
        else
            printf("socket successfully created..fd: %d\n", *sock_fd);

        bzero(&servaddr[i], sizeof(servaddr[i]));

        servaddr[i].sin_port = CLIENT_PORT + i;
        //bind this client: this is needed to set the source port
        //instead of getting a random port assigned by the OS
        if (bind(*sock_fd, (struct sockaddr*) &servaddr[i],
                 sizeof(struct sockaddr_in)) == 0)
            printf("binded correctly: on port: %d\n",servaddr[i].sin_port);
        else
            printf("unable to bind\n");

        // assign IP, PORT
        servaddr[i].sin_family = AF_INET;
        servaddr[i].sin_addr.s_addr = inet_addr(server_ip);
        servaddr[i].sin_port = htons(server_port);
        
        // connect the client socket to server socket
        if (connect(*sock_fd, (SA*)&servaddr[i], sizeof(servaddr[i])) != 0) {
            printf("connection with the server failed with error: %d: %s\n",
                   errno, strerror(errno));
            exit(0);
        }
        else
            printf("connected to the server..\n");
    }
    
    //create & start the threads
    for (int i=0; i<THREADS_NR; i++){
        //sock_fd = &(ctis[i].sock_fd);
        int ret = pthread_create(&ctis[i].thread_id,
                                 NULL, do_message_function, (void*) &ctis[i]);
    }
    //join the threads to
    for (int i=0; i<THREADS_NR; i++){
        printf("we wait for thread: [%d]\n", i);
        pthread_join(ctis[i].thread_id, NULL);
    }
    
    return 0;
}

void do_message_function(ClientThreadInfo *client_thread_info)
{
    char buf[BUF_SIZE]; 
    char *message;
    uint64_t stream_id;
    ServerResponse svr_resp;
    int sock_fd = client_thread_info->sock_fd;

    struct event *signal_event;
    struct bufferevent *bev;
    struct event_base *base;
    
    
    evutil_make_socket_nonblocking(sock_fd);
    base = event_base_new();
    if (base == NULL){
        dbg_print("Unable to create event base: %d\n", client_thread_info->idx);
        exit(1);
    }
    dbg_print("anew socket connection: %d: on fd:%d\n", client_thread_info->idx, sock_fd);
    bev = bufferevent_socket_new(base, sock_fd, BEV_OPT_CLOSE_ON_FREE);
    if (!bev) {
        fprintf(stderr, "Error constructing bufferevent!");
        event_base_loopbreak(base);
        return;
    }
    bufferevent_setcb(bev, conn_readcb, conn_writecb, conn_eventcb, (void *) client_thread_info);
    bufferevent_enable(bev, EV_WRITE | EV_READ);
    
    event_base_dispatch(base);
    event_free(signal_event);
    event_base_free(base);
}

uint64_t get_stream_id(ServerResponse *sr)
{
    return sr->signature;
}

void make_protocol_message(char *protocol, uint64_t signature, char *out_msg){
    MessageHeader *msg_hdr = (MessageHeader *)out_msg;
    bzero(out_msg, sizeof(MessageHeader));
    msg_hdr->signature = signature;
    char *cp = (char *)&msg_hdr->serial_number;
    strncpy(cp, protocol, strlen(protocol));
}

void send_protocol_bev(struct bufferevent *bev, char *protocol, uint64_t signature)
{
    char protocol_msg[MAX_PROTOCOL_MSG_SIZE];
    make_protocol_message(protocol, signature, protocol_msg);
    
    MessageHeader *hdr = (MessageHeader *)protocol_msg;
    //int written = write(sock_fd, protocol_msg, MAX_PROTOCOL_MSG_SIZE);
    
    dbg_print("sending protocol message: %u [%s]: wrote: %d bytes\n",
           hdr->serial_number, (char *)&(hdr->serial_number), MAX_PROTOCOL_MSG_SIZE);
    bufferevent_write(bev, protocol_msg, MAX_PROTOCOL_MSG_SIZE);
}

void initiate_end(struct bufferevent *bev, ClientThreadInfo *cti)
{
    send_protocol_bev(bev, PROTOCOL_MSG_END, cti->stream_id);
    bufferevent_flush(bev, EV_READ|EV_WRITE, BEV_NORMAL);
}

void initiate_mem_wait(struct bufferevent *bev, ClientThreadInfo *cti)
{
    usleep(100000); // take a pause before peeking
    cti->status = CLIENT_STATUS_PEEKING;
    send_protocol_bev(bev, PROTOCOL_MSG_PEEK, cti->stream_id);
}


void handle_peek_response(ClientThreadInfo *cti, ServerResponse *svr_resp)
{
    cti->status = CLIENT_STATUS_PEEKED;
    printf("handle_peek_response(): thread:[%d] received [%d], saved [%d], held_messages [%d]\n",
           cti->idx, svr_resp->received, svr_resp->saved, cti->held_messages_count);

    int released_messages = release_held_messages(cti, svr_resp->saved);
    
    if (svr_resp->saved + cti->held_messages_count != svr_resp->received){
        printf("we have a glitch in the matrix: saved: %d, held: %d, received: %d\n",
               svr_resp->saved, cti->held_messages_count,svr_resp->received);
        
        print_held_messages(cti);
        exit(0);
    }
    
    if (released_messages > 0)
        return;  //we released some messages, so we return immediately
    
    //We didn't release any memory, so we pause before repeating a peek
    //We can decide to wait longer if the difference between
    // "received"  and "saved" is larger
    usleep(900000);
}

void send_messages(struct bufferevent *bev, ClientThreadInfo *cti)
{
    char *message;
    dbg_print("send_messages(): index: %d\n", cti->sent_messages_idx);
    
    if (cti->stream_id == 0)
        return;
    
    while ((message = make_message(cti, cti->stream_id, cti->sent_messages_idx+1))==NULL){
        //wait_for_free_memory(cti);
        initiate_mem_wait(bev, cti);
        return;
    }
    bufferevent_write(bev, message, BUF_SIZE);
    cti->sent_messages_idx++;
    //dbg_print("sending message: %u [%d]: wrote: %d bytes\n", cti->stream_id, cti->idx, BUF_SIZE);
    //hold message in RAM till we confirm its release
    hold_message(cti, message);
}

static void
conn_readcb(struct bufferevent *bev, void *user_data)
{
    struct evbuffer *input_evbuf = bufferevent_get_input(bev);
    ClientThreadInfo *cti = (ClientThreadInfo *) user_data;
    size_t in_buf_len = evbuffer_get_length(input_evbuf);

    dbg_print("conn_readcb: [%d] is ready to read: %zu bytes\n", cti->idx, in_buf_len);
    if (in_buf_len == 0) {
        printf("failed to receive answer: %zu\n", in_buf_len);
        bufferevent_free(bev);
        return;
    }
    else{
        ServerResponse svr_resp;
        in_buf_len = bufferevent_read(bev, &svr_resp, sizeof(ServerResponse));
        
        if (cti->stream_id == 0){ //this is stream initiation
            cti->stream_id = get_stream_id(&svr_resp);
            dbg_print("conn_readcb: server_response: %d, stream_id: %llX \n",
                      cti->sock_fd, cti->stream_id);
            //initiate reading
            send_messages(bev, cti);
        }
        else { // most likely a response to peek or end
            dbg_print("conn_readcb: read from server: %zu bytes\n", in_buf_len);
            
            if (svr_resp.received == svr_resp.saved && (cti->sent_messages_idx+1) == TOTAL_PAYLOAD_COUNT){
                dbg_print("conn_readcb: looks like done: received: %d\n", svr_resp.received);
                initiate_end(bev, cti);
                bufferevent_disable(bev, EV_WRITE);
                return;
            }
            handle_peek_response(cti, &svr_resp);
        }
    }
}

static void
conn_writecb(struct bufferevent *bev, void *user_data)
{
    struct evbuffer *output = bufferevent_get_output(bev);
    int out_buf_len = evbuffer_get_length(output);
    ClientThreadInfo *cti = (ClientThreadInfo *)user_data;
    
    dbg_print("conn_writecb: [%d] is ready to write\n", cti->idx);

    if (out_buf_len == 0) {
        if (cti->stream_id == 0 && cti->status == CLIENT_STATUS_INIT){
            // we haven't initiated a stream, initiate it
            cti->status = CLIENT_STATUS_STARTING;
            send_protocol_bev(bev, PROTOCOL_MSG_START, 0LL);
        } else {  //continue sending messages
//            while (cti->status != CLIENT_STATUS_STARTED){
//                usleep(1000000);
//                dbg_print("conn_writecb: [%d] waiting for START\n", cti->idx);
//            }
            dbg_print("conn_writecb: [%d] status: %d, stream: %llX\n", cti->idx, cti->status, cti->stream_id);
            
            if ((cti->sent_messages_idx+1)  < TOTAL_PAYLOAD_COUNT){
                send_messages(bev, cti);
            }
            else {
                //bufferevent_flush(bev, EV_READ|EV_WRITE, BEV_NORMAL);
                dbg_print("conn_writecb: we are done here: sent %d messages\n", cti->sent_messages_idx);
                if (cti->status != CLIENT_STATUS_PEEKING){
                    initiate_mem_wait(bev, cti);
                }
            }
            
        }
    }
    else {
        printf("we wrote back to the client: \n");
    }
}


static void
conn_eventcb(struct bufferevent *bev, short events, void *user_data)
{
    dbg_print("connection event [%d]\n", (int) user_data);
    if (events & BEV_EVENT_EOF) {
        printf("Connection closed\n");
        
    } else if (events & BEV_EVENT_ERROR) {
        printf("Got an error on the connection: %s\n",
               strerror(errno));
    }
    else{
        printf("undocumented error, [ conn_eventcb ]\n");
    }
    /* None of the other events can happen here, since we haven't enabled
     * timeouts */
    bufferevent_free(bev);
}
