# ac_backup

Can online backup be made easier on the device?


# ac_server and ac_client

Both client and server are written on C for Linux (written on MacOS, to be tested later on Linux)

## Client Description
- Client and server communicate via TCP/IP protocol
- The client app is multithreaded (uses libpthread)
- The client app runs `N` (64) threads, each thread establishing connection to the server app 
(Started with creating socket connections outside the threads, left it there to improve debugging)
- The thread sends `TOTAL_PAYLOAD_COUNT` (5 millions) messages to the server and exits after all messages acknowledged by server.
- Size of a message is `BUF_SIZE` (8KB), message has some header and body, a message body can contain non-zero data. 
- The client holds sent messages in a per_connection linked list until acknowledgement received.
- The client app exits after all threads complete
- The size of memory allocated from heap will not exceed `MAX_ALLOCABLE_RAM` (2GB). This is regulated at the point of creation of messages (packets). The allocator will block on the thread to wait for release of RAM if it is unable to find sufficient RAM.
- Client uses libevent and non-blocking sockets

## Server Description

- The server app is also multithreaded.  
- It has single receiver thread that places packets in work queues.
- `N/3` worker threads which stores received data in `N` files (1 file per client)
- After data successfully stored on a disk the server sends ack to the client. 
- Error handling has been skimped, a little implemented for debugging purposes.
- Server uses libevent 

## Key Points for the acClient
### Performance
- We only respond to control/protocol packets
- We reduce the latency between the server and client by having the server respond immediately to a sent packet
- Time-consuming activities like loops waiting for memory availability are made to sleep to allow other threads to take over
### Memory Consumption
- The packets (messages) held in RAM are managed by a linked list
- The message headers have serial numbers 
- Each message sent to the server returns a header with the last serial number saved by the server
- The client uses the serial number to delete all earlier packets starting from the heade of the linked list
### Minimize execution time
- Execution time for such a system is ultimately bound by tne network bandwidth
- We maximize on the bandwidth by using multiple threads
- The server has been designed to respond almost immediately to a received packet
- The client can send a "PEEK" message to retrieve the status of the server and adapt accordingly


# Building the server
`$ cd ac_backup`
`make`

You will need `libevent` library in order to build this.

After a succesful build, you should open a terminal and execute the server.

`$ ./ac_server`

This will start the server on port 8888, on 127.0.0.1
These are defined as SERVER_PORT and SERVER_IP respectively in the source-code. 
You will need to chage the server IP to to 0.0.0.0 or non-local IP address in order to access from a different machine.


On a different terminal, start the client as follows: 

`$ ./ac_client`


The source files have copious `printf` statements that have been commented out. 
They were used for debugging but act as comments too. 
