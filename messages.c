//
//  messages.c
//  ac_project
//
//  Created by Bernard Adongo on 30/04/2022.
//

#include <stdlib.h>
#include <unistd.h>
#include <stdatomic.h>
#include "messages.h"

typedef struct NODE {
    char *message;
    struct NODE *next;
} Node;

static atomic_int total_held_messages = 0;

void print_held_messages(ClientThreadInfo *cti)
{
    Node *head = (Node *) cti->messages_head;
    Node *tail = (Node *) cti->messages_tail;
    
    for (Node* node=head; node!=NULL; node=node->next){
        MessageHeader *msg = (MessageHeader *)(node->message);
        
        printf("print_held_messages(): thread[%d]: serial: %d\n",
               cti->idx, msg->serial_number);
        
    }
}

/* Append message to the linked list */
void hold_message(ClientThreadInfo *cti, char *message)
{
    Node *node = (Node *)malloc(sizeof(Node));
    node->message = message;
    node->next = NULL;
    
    Node *head = (Node *) cti->messages_head;
    Node *tail = (Node *) cti->messages_tail;
    
    //printf("--hold_message() thread [%d] head: %d, tail: %d, node: %d, total_held: %d\n",
    //       cti->idx, (int)head, (int)tail, (int)node, total_held_messages);
    
    if (head == NULL){
        head = node;
        tail = head;
        
        if (cti->held_messages_count > 1){
            printf("hold_message: thread[%d]: ** glitch ** thread_held: %d\n",
                   cti->idx, cti->held_messages_count);
            exit(0);
        }
        
    }
    else{
        //append this to the tail
        tail->next = node;
        tail = node;
    }
    cti->messages_head = head;
    cti->messages_tail = tail;
    //printf("----hold_message() thread [%d] new head: %d, tail: %d\n",
    //       cti->idx, (int)head, (int)tail);

    total_held_messages++;
    
    cti->held_messages_count++;
    
    //if ((total_held_messages % 1000) == 0)
    //    printf("hold_message() thread [%d]: thread_held: %d, total_held: %d\n",
    //       cti->idx, cti->held_messages_count, total_held_messages);
    
}

/*
 Remove all the saved messages from the head of the linked list
 Returns the number of released messages
 */
int release_held_messages(ClientThreadInfo *cti, int last_saved_message)
{
    int print_logs_out = 0;
    if (total_held_messages >= MAX_ALLOCABLE_MESSAGES){
        if (cti->idx == 1)
            print_logs_out = 1;
    }
    
    int removed_messages_counter = 0;
    Node *head = (Node *) cti->messages_head;
    Node *tail = (Node *) cti->messages_tail;
    Node *node = head;
    
    while (node != NULL){
        //retrieve the serial number
        MessageHeader *msg = (MessageHeader *)(node->message);
        
        
        if (last_saved_message >= msg->serial_number){
            head = node;
            node = head->next;
            free(head->message);
            free(head);
            
            removed_messages_counter++;
        }
        else{
            break;
        }
    }
    //decrement message counter
    total_held_messages -= removed_messages_counter;
    cti->held_messages_count -= removed_messages_counter;

    if (print_logs_out){
        printf("--thread [%d]: released %d messages, thread_held: %d, total_held: %d\n",
               cti->idx, removed_messages_counter,
               cti->held_messages_count, total_held_messages);
    }
    
    if (removed_messages_counter > 0){
        cti->messages_head = node; //the head truly changed
        if (node == NULL)  //we released the tail too
            cti->messages_tail = node;
    }
    
    return removed_messages_counter;
}

/* make a buffer */
char *make_message(ClientThreadInfo *cti, uint64_t client_id, int counter)
{
    //return NULL if we've run out of memory
    if (total_held_messages >= MAX_ALLOCABLE_MESSAGES){
        printf("thread [%d] ran out of RAM...\n", cti->idx);
        return NULL;
    }
    //Using malloc, but we can use a faster allocation system
    char *buf = (char *) malloc(BUF_SIZE);
    
    MessageHeader *msg = (MessageHeader *)buf;
    msg->signature = client_id;
    msg->serial_number = counter;
    
    //fill rest of message with crap
    //for (int i=sizeof(MessageHeader); i<BUF_SIZE; i++)
    //    ((char *)buf)[i] = (cti->idx % 26) + 64;
    
    //return the message
    return buf;
}
