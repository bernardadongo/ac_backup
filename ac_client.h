//
//  ac_client.h
//  ac_project
//
//  Created by Bernard Adongo on 06/06/2022.
//

#ifndef ac_client_h
#define ac_client_h

#include "ac_server.h"



//Message holds data for a message  on the server side
typedef struct MESSAGE {
    void *data;
    uint32_t size;
    int32_t  serial;
} Message;


#define MAX_ALLOCABLE_MESSAGES   ((MAX_ALLOCABLE_RAM) / (BUF_SIZE))
#define MESSAGES_NR       ((MAX_ALLOCABLE_MESSAGES) / (THREADS_NR))

typedef struct CLIENT_THREAD_INFO {
    int         idx;  // index into the threads array - for convenient logging
    pthread_t   thread_id;  // not needed
    int         sock_fd;    // not needed, except for logging
    void       *messages_head;  //the head of linked_list store holds the sent messages
    void       *messages_tail;  //tail of the linked list store that holds unsaved messages
    int         held_messages_count; // number of unsaved messages
    int         sent_messages_idx;
    int         status;
    
//    void       *held_messages[CLIENT_MESSAGES_NR];  // array of unsaved packets
//    int         top_message_idx; //
//    int         last_message_idx;
    uint64_t    stream_id;
} ClientThreadInfo;


#define CLIENT_STATUS_INIT      0
#define CLIENT_STATUS_STARTING  1
#define CLIENT_STATUS_STARTED   2
#define CLIENT_STATUS_PEEKING   3
#define CLIENT_STATUS_PEEKED    4


#endif /* ac_client_h */
