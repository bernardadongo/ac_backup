//
//  utils.c
//  ac_project
//
//  Created by Bernard Adongo on 29/04/2022.
//

#include <stdlib.h>

#include "utils.h"

void get_stream_signature(char stream_signature[], int len)
{
    snprintf(stream_signature, len, "%02x%02x%02x%02x%02x%02x%02x%02x",
    rand(), rand(),                 // Generates a 64-bit Hex number
    rand(),                         // Generates a 32-bit Hex number
    ((rand() & 0x0fff) | 0x4000),   // Generates a 32-bit Hex number of the form 4xxx (4 indicates the UUID version)
    rand() % 0x3fff + 0x8000,       // Generates a 32-bit Hex number in the range [0x8000, 0xbfff]
    rand(), rand(), rand());
    
}

