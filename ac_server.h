//
//  ac_server.h
//  ac_project
//
//  Created by Bernard Adongo on 29/04/2022.
//

#ifndef ac_server_h
#define ac_server_h

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define SERVER_PORT   8888
//#define SERVER_IP     "127.0.0.1"
#define SERVER_IP     "0.0.0.0"
#define CLIENT_PORT   30000

#define N 2
#define THREADS_NR N

#define TOTAL_PAYLOAD_COUNT 50000 //000000


#define MAX_ALLOCABLE_RAM       ( 2 * 1020 * 1024 * 1024)
#define BUF_SIZE 8192

#define MAX_PROTOCOL_MSG_SIZE 128

#define PROTOCOL_MSG_START "START"
#define PROTOCOL_MSG_PEEK  "PEEK"
#define PROTOCOL_MSG_END   "END"
#define PROTOCOL_MSG_EXIT  "EXIT"
#define PROTOCOL_MSG_PAUSE "PAUSE"

typedef struct MESSAGE_HEADER {
    uint64_t signature;
    uint32_t serial_number;
    uint32_t padding; // unused for now
} MessageHeader;

typedef struct SERVER_RESPONSE {
    uint64_t signature;
    int32_t received;  // serial number of last received packet
    int32_t saved;     // serial number of last packet saved on the server
    int32_t optimal_qlen;  // unused for now
} ServerResponse;

#define PACKET_SIZE  (BUF_SIZE & 0x8FF) | 4096  //packet size should not be greater than 4096
#define PACKETS_SIZE TOTAL_PAYLOAD_COUNT * (BUF_SIZE / (PACKET_SIZE))  // Number of packets/messages that are assigned to each client

#define DEBUG 1

#define dbg_print(fmt, ...) \
            do { if (DEBUG) fprintf(stderr, fmt, __VA_ARGS__); } while (0)


#endif /* ac_server_h */
