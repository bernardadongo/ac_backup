//
//  workers.c
//  ac_project
//
//  Created by Bernard Adongo on 30/04/2022.
//

#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <string.h>
#include <sys/uio.h>
#include "ac_server.h"
#include "workers.h"
#include "process_data.h"


#define WORKERS_NR  THREADS_NR/3 + 1

#define IOVEC_SIZE 128

pthread_t thread_id[WORKERS_NR];
pthread_mutex_t worker_mutex;


void init_workers();
void *do_worker_function(void *);

int ids[WORKERS_NR];

int start_worker_threads()
{
    pthread_mutex_init(&worker_mutex, NULL);

    for (int i=0; i<WORKERS_NR; i++){
        printf("creating worker threads for: %d\n", i);
        ids[i] = i;
        int ret = pthread_create(&thread_id[i], NULL,
                                 do_worker_function, (void *)&ids[i]);
    }

    for (int i=0; i<WORKERS_NR; i++){
        printf("we detach the worker thread: [%d]\n", i);
        pthread_detach(thread_id[i]);
    }
    
    return 0;
}

void *do_worker_function(void *thread_idx){
    ProgressInfo *pi;
    struct iovec  iov[IOVEC_SIZE];
    Packet       *packets[IOVEC_SIZE];
    int           pkt_count=0;


    int log_counter = 0;
    int idx = *((int *)thread_idx);
    printf("worker_thread at [%d] started\n", idx);

    //sleep-wait for progress
    while (!progress_is_available()){
        if ((log_counter % 1000) == 0)
            printf("waiting for progress: [%d]\n", idx);
        usleep(10000);
        log_counter++;
    }

    //processor loop
    for(;;){
        pthread_mutex_lock(&worker_mutex);
        ProgressInfo *pi = get_next_progress_info();
        pthread_mutex_unlock(&worker_mutex);
        
        if (pi != NULL){
            int   write_buf_len;
            int   total_write_buf_len = 0;
            char *write_buf;
            int   cached_idx = 0;
            int   from_saved_idx = 0;
            
            //retrieve packets
            while (pi->packet_buf_saved_idx == pi->packet_buf_idx){
                dbg_print("do_worker_function(): worker [%d] sleep_waiting for client packet\n",
                          idx);
                usleep(1000000);
            }
            
            from_saved_idx = pi->packet_buf_saved_idx;
            //cached because writer thread updates this values
            cached_idx = pi->packet_buf_idx;
            
            //Packet *packet = get_pi_next_save_packet(pi);
            pkt_count = 0;
            while (pi->packet_buf_saved_idx != cached_idx){
                Packet *pkt = packets[pkt_count] = get_pi_next_save_packet(pi);
                
                if (pkt->type == PACKET_TYPE_TOP){
                    write_buf_len = pkt->size - sizeof(MessageHeader);
                    write_buf = pkt->data+sizeof(MessageHeader);
    //                MessageHeader *hdr = (MessageHeader *)pkt->data;
    //                printf("to top write: %llX:%llX to fd: %d, len: %d: serial: %d\n", hdr->signature,
    //                      pi->signature, pi->file_fd, write_buf_len, packet->serial);
                }
                else{
                    write_buf_len = pkt->size; //bottom packet has no msg header
                    write_buf = pkt->data;
    //                printf("to bot write: %llX to fd: %d, len: %d serial: %d\n",
    //                       pi->signature, pi->file_fd, write_buf_len, packet->serial);
                }
                
                iov[pkt_count].iov_base = write_buf;
                iov[pkt_count].iov_len = write_buf_len;
                
                total_write_buf_len += write_buf_len;
                
                pkt_count++;
                
                if (pkt_count == IOVEC_SIZE) break;
            }
            
            
            dbg_print("do_worker_function(): to write fd: %d, iovs: %d, size: %d, from: %d, to: %d, latest_saved: %d, latest_idx: %d\n", pi->file_fd, pkt_count, total_write_buf_len, from_saved_idx,
                      cached_idx,
                      pi->packet_buf_saved_idx,
                            pi->packet_buf_idx);
            
            if (pkt_count <= 1){
                dbg_print("do_worker_function(): Error: pkt_count: %d: latest_iov_base: %d, size: %d\n",
                          pkt_count,
                          (int)&iov[0].iov_base, iov[0].iov_len);
                
            }
            
            int written_len = writev(pi->file_fd, iov, pkt_count);
        
            
            if (written_len < 0){
                dbg_print("do_worker_function(): Got an error on writing file: %s\n",strerror(errno));
                dbg_print("do_worker_function(): Error: latest_iov: %d, size: %d\n",
                          (int)&iov[pkt_count-1].iov_base, iov[pkt_count-1].iov_len);
//                dbg_print("do_worker_function(): fd: %d, iovs: %d, total_write_buf_len: %d\n", pi->file_fd,
//                          pkt_count, total_write_buf_len);
                exit(1);
            }
            //else if (written_len == write_buf_len){
            else if (written_len == total_write_buf_len){
                
                Packet *packet;
                //we release packet when we know data is on disk
                fsync(pi->file_fd);
                
                pi->saved = packets[pkt_count-1]->serial;
                
                //only update 'saved' if the bottom packet was saved
                for (int i=0; i<pkt_count; i++){
                    packet = packets[i];
                    
                    packet->type = PACKET_TYPE_TOP; //
                    packet->serial = 0;
                    packet->size = 0;
                    release_packet_data(packet);
                }

                pi->saving = 0;
            }
            else{
                //this was an error
                printf("error writing to file\n");
            }
            continue;
        }
        //we reached here because we need to wait..
        usleep(1000000);
    }
    return 0;
}

