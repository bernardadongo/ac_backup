LDFLAGS = -lpthread -levent -levent_pthreads -g 
INCS = process_data.h utils.h 


SRCS=$(wildcard *.c)

OBJS=$(SRCS:.c=.o)

all: ac_server ac_client $(OBJS)


ac_server: $(filter-out ac_client.o, $(OBJS))
ac_client: $(filter-out ac_server.o, $(OBJS))

clean:
	rm ac_server ac_client $(OBJS)


