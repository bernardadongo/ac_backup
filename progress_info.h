//
//  progress_info.h
//  ac_project
//
//  Created by Bernard Adongo on 13/06/2022.
//

#ifndef progress_info_h
#define progress_info_h

#include <stdio.h>

#define PACKET_TYPE_TOP 0x00
#define PACKET_TYPE_BOTTOM 0x01

//Packet holds data for a Message (called packet) on the server side
//If the message exceeds 4KB, the message will be split into two packets
typedef struct PACKET {
    void *data;
    uint32_t size;
    int32_t  serial;
    uint32_t type;
} Packet;

/*
 ProgressInfo is used by the server to keep track of a stream's
 progress. Basically on object is created for each file being written to.
 */
typedef struct PROGRESS_INFO {
    uint64_t signature;  // Stream ID
    int32_t received;    //serial number of last received packet
    int32_t saved;       //serial number of last saved packet
    int32_t saving;      //this is set to the "saved" that we are saving
    uint32_t optimal_qlen;   // not used at the moment
    int32_t packet_buf_idx;  // index of the last received packet on
    int32_t packet_buf_saved_idx;  // index of the last saved packet info
    int file_fd;
    int sock_fd;
    int32_t conn_idx;        // associated connection idx
    int32_t idx;        //index inside the list of ProgressInfo[N]
    Packet  *packets; //array that holds Packets storage
} ProgressInfo;

ProgressInfo* create_progress_info(int conn_idx, uint64_t signature);

/*
 Connection info holds current info about each connection
 */
typedef struct CONNECTION_INFO {
    int             conn_id; // a simple index for now
    // Where BUF_SIZE exceeds 4KB, packet is divided into TOP and BOTTOM halves
    int             packet_type;
    ProgressInfo    *pi;
    ServerResponse  *sr;
} ConnectionInfo;

void init_connection_infos();

extern ConnectionInfo *conn_infos;

#endif /* progress_info_h */
