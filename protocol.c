//
//  protocol.c
//  ac_project
//
//  Created by Bernard Adongo on 13/06/2022.
//
#include "ac_server.h"
#include "progress_info.h"
#include "protocol.h"
#include "utils.h"
#include <string.h>

int is_message_start(MessageHeader *msg_hdr);
int is_message_peek(MessageHeader *msg_hdr);
int is_message_end(MessageHeader *msg_hdr);

void svr_resp_from_pi(ServerResponse *sr, ProgressInfo *pi);


int process_protocol_message(int conn_idx, char *in_buf, char **out_buf){
    ConnectionInfo *ci = &conn_infos[conn_idx];
    
    //START command: Returns the stream signature
    if (is_message_start((MessageHeader *)in_buf)){
        get_stream_signature((char *)&ci->sr->signature,
                            sizeof(ci->sr->signature));
        
        dbg_print("process_protocol_message(): new signature: %llX\n",
                  ci->sr->signature);
        ProgressInfo *pi = create_progress_info(conn_idx, ci->sr->signature);
        conn_infos[conn_idx].pi = pi;
        svr_resp_from_pi(ci->sr, pi);
        *out_buf = (char *)&ci->sr;
    }
    //PEEK command: we start off closing this stream
    else if (is_message_peek((MessageHeader *)in_buf)){
        ProgressInfo* pi = ci->pi;

        svr_resp_from_pi(ci->sr, pi);
        
        MessageHeader *msg_hdr = (MessageHeader *)in_buf;
        dbg_print("process_protocol_packet(): peek [%llX]: received[%d], saved: [%d]\n",
               msg_hdr->signature, pi->received, pi->saved);
        
        *out_buf = (char *)&ci->sr;
    }
    //END command: we start off closing this stream
    else if (is_message_end((MessageHeader *)in_buf)){
        MessageHeader *msg_hdr = (MessageHeader *)in_buf;

        ProgressInfo* pi = ci->pi;
        dbg_print("process_protocol_packet(): END [%llX]: idx [%d]\n",
               msg_hdr->signature,  pi->packet_buf_idx);
        
        *out_buf = (char *)&ci->sr;
    }
    else{
        dbg_print("process_protocol_packet(): connection [%d] Unknown command, we should never get here\n",
                  conn_idx);
    }
    
    return sizeof(ServerResponse);
}

void svr_resp_from_pi(ServerResponse *sr, ProgressInfo *pi)
{
    sr->signature = pi->signature;
    sr->received = pi->received;
    sr->saved = pi->saved;
    sr->optimal_qlen = pi->optimal_qlen;
}

int is_message_start(MessageHeader *msg_hdr)
{
    char* msg = (char *)&(msg_hdr->serial_number);
    return (strncmp(msg, PROTOCOL_MSG_START, sizeof(PROTOCOL_MSG_START))==0);
}

int is_message_peek(MessageHeader *msg_hdr)
{
    char* msg = (char *)&(msg_hdr->serial_number);
    return (strncmp(msg, PROTOCOL_MSG_PEEK, sizeof(PROTOCOL_MSG_PEEK))==0);
}

int is_message_end(MessageHeader *msg_hdr)
{
    char* msg = (char *)&(msg_hdr->serial_number);
    return (strncmp(msg, PROTOCOL_MSG_END, sizeof(PROTOCOL_MSG_END))==0);
}
