//
//  messages.h
//  ac_project
//
//  Created by Bernard Adongo on 30/04/2022.
//

#ifndef messages_h
#define messages_h

#include <stdio.h>
#include "ac_client.h"

void hold_message(ClientThreadInfo *cti, char *buf);
int release_held_messages(ClientThreadInfo *cti, int last_saved_message);
char *make_message(ClientThreadInfo *cti, uint64_t client_id, int counter);
void print_held_messages(ClientThreadInfo *cti);


#endif /* messages_h */
