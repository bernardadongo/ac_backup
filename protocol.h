//
//  protocol.h
//  ac_project
//
//  Created by Bernard Adongo on 13/06/2022.
//

#ifndef protocol_h
#define protocol_h

#include <stdio.h>

int process_protocol_message(int conn_idx, char *in_buf, char **out_buf);

#endif /* protocol_h */
