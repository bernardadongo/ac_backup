//
//  utils.h
//  ac_project
//
//  Created by Bernard Adongo on 29/04/2022.
//

#ifndef utils_h
#define utils_h

#include <stdio.h>

void get_stream_signature(char stream_signature[], int len);

#endif /* utils_h */
