//
//  process_data.h
//  ac_project
//
//  Created by Bernard Adongo on 30/04/2022.
//

#ifndef process_data_h
#define process_data_h

#include <stdio.h>
#include "progress_info.h"

int process_protocol_message(int conn_idx, char *in_buf, char **out_buf);

int process_packet(int conn_idx, char *in_buf, char **out_buf, size_t in_buf_len);

void delete_progress_info(uint64_t signature);


int progress_is_available();
ProgressInfo *get_next_progress_info();

void release_packet_data(Packet *packet);
Packet *get_pi_next_save_packet(ProgressInfo *pi);

void init_progress_infos();


#endif /* process_data_h */
