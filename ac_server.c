//
//  ac_server.c
//  ac_project
//
//  Created by Bernard Adongo on 29/04/2022.
//

#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#ifndef _WIN32
#include <netinet/in.h>
# ifdef _XOPEN_SOURCE_EXTENDED
#  include <arpa/inet.h>
# endif
#include <sys/socket.h>
#endif
#include <time.h>

#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/listener.h>
#include <event2/util.h>
#include <event2/event.h>

#include "ac_server.h"
#include "process_data.h"
#include "workers.h"


static void listener_cb(struct evconnlistener *, evutil_socket_t,
    struct sockaddr *, int socklen, void *);
static void conn_writecb(struct bufferevent *, void *);
static void conn_readcb(struct bufferevent *, void *);
static void conn_eventcb(struct bufferevent *, short, void *);
static void signal_cb(evutil_socket_t, short, void *);
static void accept_error_cb(struct evconnlistener *listener, void *ctx);

int start_net_server();

int server_port = SERVER_PORT;

int main(int argc, char **argv)
{
    int ret_val;

    if (argc > 1)
      server_port = atoi(argv[1]);

    printf("ac_server %d\n", server_port); 
    
    init_connection_infos();

    ret_val = start_worker_threads();
    ret_val = start_net_server();

    return ret_val;
}

int start_net_server()
{
    struct event_base *base;
    struct evconnlistener *listener;
    struct event *signal_event;

    struct sockaddr_in sin = {0};

    srand(time(NULL)); //randoms
    
    base = event_base_new();
    if (!base) {
        fprintf(stderr, "Could not initialize libevent!\n");
        return 1;
    }

    sin.sin_family = AF_INET;
    sin.sin_port = htons(server_port);

    listener = evconnlistener_new_bind(base, listener_cb, (void *)base,
        LEV_OPT_REUSEABLE|LEV_OPT_CLOSE_ON_FREE, -1,
        (struct sockaddr*)&sin,
        sizeof(sin));

    if (!listener) {
        fprintf(stderr, "Could not create a listener!\n");
        return 1;
    }

    evconnlistener_set_error_cb(listener, accept_error_cb);
    
    signal_event = evsignal_new(base, SIGINT, signal_cb, (void *)base);

    if (!signal_event || event_add(signal_event, NULL)<0) {
        fprintf(stderr, "Could not create/add a signal event!\n");
        return 1;
    }

    event_base_dispatch(base);
    evconnlistener_free(listener);
    event_free(signal_event);
    event_base_free(base);
    
    return 0;
}


int g_connections_count = 0;

static void
listener_cb(struct evconnlistener *listener, evutil_socket_t fd,
    struct sockaddr *sa, int socklen, void *user_data)
{
    struct event_base *base = user_data;
    struct bufferevent *bev;
    
    dbg_print("new socket connection: %d: on fd:%d\n", g_connections_count, fd);
    bev = bufferevent_socket_new(base, fd, BEV_OPT_CLOSE_ON_FREE);
    if (!bev) {
        fprintf(stderr, "Error constructing bufferevent!");
        event_base_loopbreak(base);
        return;
    }
    
    bufferevent_setcb(bev, conn_readcb, conn_writecb,
                      conn_eventcb, (void *)g_connections_count);
    bufferevent_enable(bev, EV_WRITE | EV_READ);
    
    g_connections_count++;
}

static void
conn_readcb(struct bufferevent *bev, void *user_data)
{
    char *out_buf;
    int conn_idx = (int)(user_data);
    int out_buf_len;
    struct evbuffer *input_evbuf = bufferevent_get_input(bev);
    
    size_t in_buf_len = evbuffer_get_length(input_evbuf);
    
    if (in_buf_len == 0) {
        dbg_print("conn_readcb(): conn[%d], 0 length read buffer: %zu\n", conn_idx, in_buf_len);
        bufferevent_free(bev);
        return;
    }
    
    if (in_buf_len <= MAX_PROTOCOL_MSG_SIZE){
        char *in_buf = (char *)malloc(in_buf_len);

        in_buf_len = bufferevent_read(bev, in_buf, in_buf_len);
        // This is a protocol messsage
        MessageHeader *hdr = (MessageHeader *)in_buf;
        dbg_print("conn[%d]: received protocol message: Length: %d serial: %u [%s]\n",
               conn_idx,
               in_buf_len,
               hdr->serial_number, (char *)&(hdr->serial_number));
        
        out_buf_len = process_protocol_message(conn_idx, in_buf, &out_buf);
        
        //write a response to client
        bufferevent_write(bev, out_buf, out_buf_len);
        free(in_buf);
    }
    else{
        char *in_buf = (char *)malloc(BUF_SIZE);

        //This must be a packet
        in_buf_len = bufferevent_read(bev, in_buf, in_buf_len);
        out_buf_len = process_packet(conn_idx, in_buf, &out_buf, in_buf_len);
    }
    

}




static void
conn_writecb(struct bufferevent *bev, void *user_data)
{
    struct evbuffer *output = bufferevent_get_output(bev);
    if (evbuffer_get_length(output) == 0) {
        //printf("ready to write\n");
        //bufferevent_free(bev);
    }
    else {
        printf("we wrote back to the client: \n");
    }
}


static void
conn_eventcb(struct bufferevent *bev, short events, void *user_data)
{
    if (events & BEV_EVENT_EOF) {
        printf("Connection closed\n");
        g_connections_count--;
        if (g_connections_count == 0)
            exit(0);
        
    } else if (events & BEV_EVENT_ERROR) {
        printf("Got an error on the connection: %s\n",
               strerror(errno));
    }
    else{
        printf("undocumented error, [ conn_eventcb ]\n");
    }
    /* None of the other events can happen here, since we haven't enabled
     * timeouts */
    bufferevent_free(bev);
}


static void
signal_cb(evutil_socket_t sig, short events, void *user_data)
{
    struct event_base *base = user_data;
    struct timeval delay = { 2, 0 };

    printf("Caught an interrupt signal; exiting cleanly in two seconds.\n");

    event_base_loopexit(base, &delay);
}


static void
accept_error_cb(struct evconnlistener *listener, void *ctx)
{
    struct event_base *base = evconnlistener_get_base(listener);
    int err = EVUTIL_SOCKET_ERROR();
    fprintf(stderr, "Got an error %d (%s) on the listener. "
            "Shutting down.\n", err, evutil_socket_error_to_string(err));

    event_base_loopexit(base, NULL);
}
