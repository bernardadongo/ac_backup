//
//  progress_info.c
//  ac_project
//
//  Created by Bernard Adongo on 13/06/2022.
//

#include "ac_server.h"
#include "progress_info.h"
#include <fcntl.h>
#include <errno.h>
#include <strings.h>
#include <unistd.h>


ConnectionInfo conn_info_store[N];
ConnectionInfo *conn_infos;

ProgressInfo progress_infos[N];
volatile int g_progress_info_counter = 0;

/*  We are cheating here but
    Ideally, we should confirm that we don't exist then allocate
    dynamically, but we don't
*/
ProgressInfo *alloc_progress_info(int conn_idx, uint64_t signature){
    dbg_print("alloc_progress_info(): initiazlize progress_info [%d]\n",
           conn_idx);
    ProgressInfo *pi = &progress_infos[conn_idx];
    //clear content
    bzero(pi, sizeof(ProgressInfo));
    pi->idx = g_progress_info_counter;
    pi->conn_idx = conn_idx;
    pi->packet_buf_idx = -1;
    pi->received = -1;
    pi->saved = -1;
    pi->packet_buf_saved_idx = -1;
    pi->signature = signature;
    pi->packets = malloc(sizeof(Packet) * PACKETS_SIZE);

    //increment this
    g_progress_info_counter = g_progress_info_counter + 1;

    return pi;
}

/*
 We need the signature in order to create this connection
 */
ProgressInfo* create_progress_info(int conn_idx, uint64_t signature)
{
    //retrieve a progress info for this
    ProgressInfo *pi = alloc_progress_info(conn_idx, signature);
    pi->optimal_qlen = 100; //not used for now
    
    //open destination file
    char filename[64];
    snprintf(filename, 64, "data_%llX", signature);
    pi->file_fd = open(filename, O_CREAT|O_RDWR);
    dbg_print("opened file: [fd: %d] %s\n", pi->file_fd, filename);
    if (pi->file_fd < 0) {
        dbg_print("Got an error on the file: %s\n", strerror(errno));
    }
    
    return pi;
}

/*
 For now, basically remove from the list
 */
void delete_progress_info(uint64_t signature)
{
    ProgressInfo *pi = NULL;
    for (int i=0; i<N; i++){
        if (signature == progress_infos[i].signature){
            progress_infos[i].signature = 0;
            close(progress_infos[i].file_fd);
        }
    }
}

//Used to check whether progress is available
int progress_is_available(){
    return g_progress_info_counter;
}
/*
 Loop through the ProgressInfo list and return
 ProgressInfo to be saved next.
 We MARK this ProgressInfo as BUSY by setting "saving" the
 index of the packet to be saved
 */
ProgressInfo *get_next_progress_info()
{
    static int progress_idx = -1;
    ProgressInfo *pi;
    
    dbg_print("get_next_progress_info(): g_progress_info_counter: [%d]\n",
              g_progress_info_counter);
    if (g_progress_info_counter == 0){
        
        return NULL;
    }
    while(1){
        progress_idx += 1;
        progress_idx %= g_progress_info_counter;
        
        pi = &progress_infos[progress_idx];
        
        if ((pi->saving == 0) && (pi->received > pi->saved)){
            pi->saving = pi->saved + 1;
            dbg_print("get_next_progress_info(): new progress idx: [%d]\n",
                      progress_idx);
            return pi;
        }
        //we should never get here, if we do, most likely the workers
        //are being too efficient, so we wait a bit before next iteration
        usleep(10000);
    }
    
    return pi;
}

/*
 Retrieve the next packet.
 For simplicity, we are using a fixed array for packet storage
 */
Packet *get_pi_next_save_packet(ProgressInfo *pi)
{
    pi->packet_buf_saved_idx++;
    pi->packet_buf_saved_idx %= PACKETS_SIZE;
//    dbg_print("get_pi_next_save_packet: [%d]: returning packet: %d\n",
//              pi->idx, pi->packet_buf_saved_idx);
    return &(pi->packets[pi->packet_buf_saved_idx]);
}

void release_packet_data(Packet *packet)
{
    free(packet->data);
}

void init_connection_infos()
{
    conn_infos = (ConnectionInfo *)conn_info_store;
    
    for (int i=0; i<N; i++){
        conn_infos[i].pi = NULL;
        conn_infos[i].packet_type = PACKET_TYPE_TOP;
        conn_infos[i].conn_id=-1;
        conn_infos[i].sr = malloc(sizeof(ServerResponse));
    }
    
    
}
